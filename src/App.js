import './App.css';
import { ChoiceRecord } from './ChoiceRecord/ChoiceRecord';
import { Header } from './header/Header';



const App = ()  => {
  return (
    <div className='app'>
      <Header />
      <ChoiceRecord />
      
    </div>
  )
}

export default App;
