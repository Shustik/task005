import React, { useState } from "react";
import { RecordAudio } from "../RecordAudio/RecordAudio";
import { RecordVideo } from "../RecordVideo/RecordVideo";
import s from './ChoiceRecord.module.css'

export const ChoiceRecord = () => {
    const [choice, setChoice] = useState(null)
    if (choice === 'audio') {
        return <RecordAudio />
    }
    else if (choice === 'video') {
        return <RecordVideo />
    }
    return (
        <div className={s.container}>
            <div onClick={() => setChoice('audio')}>
                <img className={s.img} src="https://lumpics.ru/wp-content/uploads/2017/10/Kak-zapisat-audio-onlayn.png" alt="#" />
                
            </div>
            <div onClick={() => setChoice('video')} >
                <img className={s.img} src="https://cdn-icons-png.flaticon.com/512/5225/5225451.png" alt="" />
                
            </div>
        </div>
    )
}