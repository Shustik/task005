import React, { useState, useEffect } from "react";
import s from './Record.module.css'



export const RecordVideo = () => {
  const [audio, setAudio] = useState(null)
  const [audioUrl, setAudioUrl] = useState(null)
  const [state, setState] = useState(true)
  useEffect(() => {
    if (!audio) {
      navigator.mediaDevices.getUserMedia({ video: true })
        .then(stream => {
          const mediaRecorder = new MediaRecorder(stream)
          setAudio(mediaRecorder);
        })
    }
  }, [audio])

  if (!audio) {
    return null
  }
  const audioChunks = []
  audio.ondataavailable = (event) => {
    audioChunks.push(event.data)
  };
  audio.onstart = () => {
    setState(false)
  }
  audio.onstop = () => {
    const audioBlob = new Blob(audioChunks, {
      type: 'video/mp4'
    });
    setAudioUrl(URL.createObjectURL(audioBlob))
    setState(true)
  }

  return (
    <div className={s.container}>
      <div className={s.btn}>
      </div>
      {state ? <button onClick={() => { audio.start() }} className={s.buttonRecord} >Recording</button>
        : <button onClick={() => { audio.stop() }} className={s.buttonStop} >Stop</button>}

      <div>
        <video src={audioUrl} className={s.audio} controls></video>
      </div>
    </div>
  )
}






